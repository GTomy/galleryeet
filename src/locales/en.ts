export const EN = {
  title: 'GallerYeet',
  subtitle: 'a place where you can find photos and videos from my travels',
  description:
    'Welcome to our travel and photography blog! Your guide to the world where adventure meets art. On this blog, I will introduce you to the fascinating world of travel and photography. Together, we will explore endless possibilities on how to capture the beauty, magic, and uniqueness of the places we visit.',
  home: 'Home',
  view: 'View',
  back: 'Back',
  gallery: {
    title: 'Gallery',
    noGalleries: 'No galleries were found.',
  },
  posts: {
    title: 'Posts',
    noPosts: 'No posts were found.',
  },
  instax: {
    title: 'Instax',
  },
  admin: {
    title: 'Admin',
    create: 'Create',
    createGallery: 'Create gallery',
    galleries: 'Galleries',
    add: 'Add',
    delete: 'Delete',
    inputTitle: 'Title',
    photo: 'Photo',
    description: 'Description',
    thumbnail: 'Thumbnail',
    contents: 'Contents',
    edit: 'Edit',
  },
  footer: {
    title: 'GallerYeet',
    subtitle: 'Created by GTomy, 2024',
    projects: 'Projects',
    usefulLinks: 'Useful links',
  },
};
