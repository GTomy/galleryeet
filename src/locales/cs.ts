export const CS = {
  title: 'GallerYeet',
  subtitle: 'místo kde najdete fotky i videa z mých cest',
  description:
    'Vítejte na našem cestovatelském a fotografickém blogu! Váš průvodce světem, kde se snoubí dobrodružství s uměním. Na tomto blogu vás zasvětím do fascinujícího světa cestování a fotografování. Společně prozkoumáme nekonečné možnosti, jak zachytit krásu, kouzlo a jedinečnost míst, která navštívíme.',
  home: 'Domů',
  view: 'Zobrazit',
  back: 'Zpět',
  gallery: {
    title: 'Galerie',
    noGalleries: 'Žádné galerie nebyly nalezeny.',
  },
  posts: {
    title: 'Příspěvky',
    noPosts: 'Žádné příspěvky nebyly nalezeny.',
  },
  instax: {
    title: 'Instax',
  },
  admin: {
    title: 'Admin',
    create: 'Vytvořit',
    createGallery: 'Vytvořit galerii',
    galleries: 'Galerie',
    add: 'Přidat',
    delete: 'Smazat',
    inputTitle: 'Nadpis',
    photo: 'Fotka',
    description: 'Popis',
    thumbnail: 'Thumbnail',
    contents: 'Obsah',
    edit: 'Upravit',
  },
  footer: {
    title: 'GallerYeet',
    subtitle: 'Vytvořil GTomy, 2024',
    projects: 'Projekty',
    usefulLinks: 'Užitečné odkazy',
  },
};
