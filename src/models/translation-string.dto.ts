type TranslationStringKey = 'cs' | 'en';
export type TranslationString = {
  [key in TranslationStringKey]: string;
};
